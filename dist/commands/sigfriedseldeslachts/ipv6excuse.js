'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getIPv6Excuse = async message => {
    if (message.content === "!ipv6excuse") {
        try {
            const { data } = await _axios2.default.get('https://ipv6excuses.com/');
            const $ = await _cheerio2.default.load(data);

            await message.channel.send($('h1').text());
        } catch (error) {
            message.channel.send(error);
        }
    }
};

exports.default = getIPv6Excuse;