'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getDogPictureCommand = message => {
    if (message.content === "!dog") {
        _axios2.default.get('https://api.thedogapi.com/v1/images/search', {
            headers: {
                'x-api-key': process.env.CAT_API_KEY
            }
        }).then(response => {
            message.channel.send(response.data[0].url);
        }).catch(() => {
            message.channel.send('Woef woef, ik kon de hond niet versturen.');
        });
    }
};

exports.default = getDogPictureCommand;