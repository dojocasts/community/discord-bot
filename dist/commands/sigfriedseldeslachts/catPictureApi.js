'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getCatPictureCommand = async message => {
    if (message.content === "!cat") {
        try {
            const { data } = await _axios2.default.get('https://api.thecatapi.com/v1/images/search', {
                headers: { 'x-api-key': process.env.CAT_API_KEY }
            });

            message.channel.send(data[0].url);
        } catch (error) {
            message.channel.send('Mwaaauuwwww, ik kon de kat niet versturen.');
        }
    }
};

exports.default = getCatPictureCommand;