'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _discord = require('discord.js');

var _register = require('./register.js');

var _register2 = _interopRequireDefault(_register);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @param  {object} message
 */
const showHelpMessage = message => {
  if (message.content === '!help') {
    /**
     * Create a new rich embed
     */
    const rEmbed = new _discord.RichEmbed();

    /**
     * Register every entry
     */
    _register2.default.forEach(commandField => {
      rEmbed.addField(commandField.command, commandField.description);
    });

    /**
     * Send the rich embed
     */
    message.channel.send(rEmbed);
  }
};

exports.default = showHelpMessage;