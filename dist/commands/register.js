'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _index = require('./help/index');

var _index2 = _interopRequireDefault(_index);

var _catPictureApi = require('./sigfriedseldeslachts/catPictureApi');

var _catPictureApi2 = _interopRequireDefault(_catPictureApi);

var _dogPictureApi = require('./sigfriedseldeslachts/dogPictureApi');

var _dogPictureApi2 = _interopRequireDefault(_dogPictureApi);

var _ipv6excuse = require('./sigfriedseldeslachts/ipv6excuse');

var _ipv6excuse2 = _interopRequireDefault(_ipv6excuse);

var _doubtImage = require('./sigfriedseldeslachts/doubtImage');

var _doubtImage2 = _interopRequireDefault(_doubtImage);

var _wutImage = require('./sigfriedseldeslachts/wutImage');

var _wutImage2 = _interopRequireDefault(_wutImage);

var _coinflip = require('./sigfriedseldeslachts/coinflip');

var _coinflip2 = _interopRequireDefault(_coinflip);

var _eightball = require('./sigfriedseldeslachts/eightball');

var _eightball2 = _interopRequireDefault(_eightball);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @description Place your command functions in here
 * @param {object} message
 * @param {object} client
 * @returns void
 */
const callFunctions = async (message, client) => {
  (0, _index2.default)(message);

  /**
   * Add your command functions below
   */
  (0, _catPictureApi2.default)(message);
  (0, _dogPictureApi2.default)(message);
  (0, _ipv6excuse2.default)(message);
  (0, _doubtImage2.default)(message);
  (0, _wutImage2.default)(message);
  (0, _coinflip2.default)(message);
  (0, _eightball2.default)(message);
};

exports.default = callFunctions;