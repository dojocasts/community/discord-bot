'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * DO NOT CHANGE ANYTHING IN THIS FILE
 * 
 * @author Sigfried Seldeslachts
 */
const Discord = require('discord.js');

/**
 * Create client
 */
const client = new Discord.Client();

/**
 * Export client
 */
exports.default = client;