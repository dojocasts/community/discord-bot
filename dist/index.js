'use strict';

var _client = require('./init/client');

var _client2 = _interopRequireDefault(_client);

var _register = require('./commands/register');

var _register2 = _interopRequireDefault(_register);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * DO NOT CHANGE ANYTHING IN THIS FILE
 * 
 * @author Sigfried Seldeslachts
 */
require('dotenv').config();

/**
 * Import files
 */


/**
 * Register the commands
 */
_client2.default.on('message', message => {
  (0, _register2.default)(message, _client2.default);
});

/**
 * Server greeting
 */
_client2.default.on('guildMemberAdd', member => {
  const channel = member.guild.channels.find(channel => {
    channel.name === 'main_chat';
  });

  if (channel) {
    channel.send(`Welkom ${member} op de server!`);
  }
});

/**
 * Login
 */
_client2.default.login(process.env.DISCORD_TOKEN);