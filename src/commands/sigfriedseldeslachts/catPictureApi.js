import axios from 'axios'

const getCatPictureCommand = async (message) => {
    if (message.content === "!cat") {
        try {
            const { data } = await axios.get('https://api.thecatapi.com/v1/images/search', {
                headers: { 'x-api-key': process.env.CAT_API_KEY }
            })

            message.channel.send(data[0].url)
        } catch (error) {
            message.channel.send('Mwaaauuwwww, ik kon de kat niet versturen.')
        }
    }
}

export default getCatPictureCommand