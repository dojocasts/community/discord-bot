import axios from "axios"

export default async (message) => {
    if (message.content.startsWith("!eightball")) {
        try {
            const { data } = await axios.get('https://yesno.wtf/api')

            message.reply([
                data.answer,
                data.image
            ])
        } catch (e) {
            message.channel.send([
                'Ooopsieee, een foutje! Ik kan dus niet antwoorden. :(',
                e
            ])
        }
    }
}