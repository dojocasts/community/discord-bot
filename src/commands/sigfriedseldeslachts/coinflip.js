export default (message) => {
    if (message.content.startsWith('!coinflip')) {
        const result = (Math.floor(Math.random() * 2) == 0) ? 'Heads (kop)' : 'Tails (munt)';

        message.reply(result);
    }
}