import axios from 'axios'
import cheerio from 'cheerio'

const getIPv6Excuse = async (message) => {
    if (message.content === "!ipv6excuse") {
        try {
            const { data } = await axios.get('https://ipv6excuses.com/')
            const $ = await cheerio.load(data)

            await message.channel.send($('h1').text())
        } catch (error) {
            message.channel.send(error)
        }
    }
}

export default getIPv6Excuse