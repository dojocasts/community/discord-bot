const showDoubtImage = async (message) => {
    if (message.content.startsWith("!doubt")) {
        /**
         * Get the first user which is mentioned
         */
        const user = message.mentions.users.first()
        const member = message.guild.member(user)

        if (user && member) {
            const userWhoDoubts = message.member.user

            message.channel.send([
                `<@${user.id}>, <@${userWhoDoubts.id}> betwijfelt dat!`,
                'https://sigfriedsimages.b-cdn.net/doubt.png'
            ]);
        } else {
            message.reply([
                'jij betwijfelt dat!',
                'https://sigfriedsimages.b-cdn.net/doubt.png'
            ])
        }

        message.delete()
    }
}

export default showDoubtImage