import axios from 'axios'

const getDogPictureCommand = (message) => {
    if (message.content === "!dog") {
        axios.get('https://api.thedogapi.com/v1/images/search', {
                headers: {
                    'x-api-key': process.env.CAT_API_KEY
                }
            }).then((response) => {
                message.channel.send(response.data[0].url)
            }).catch(() => {
                message.channel.send('Woef woef, ik kon de hond niet versturen.')
            })
    }
}

export default getDogPictureCommand