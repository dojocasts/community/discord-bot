import { RichEmbed } from 'discord.js'

import commandFields from './register.js'

/**
 * @param  {object} message
 */
const showHelpMessage = (message) => {
    if (message.content === '!help') {
        /**
         * Create a new rich embed
         */
        const rEmbed = new RichEmbed()

        /**
         * Register every entry
         */
        commandFields.forEach((commandField) => {
            rEmbed.addField(commandField.command, commandField.description)
        })

        /**
         * Send the rich embed
         */
        message.channel.send(rEmbed)
    }
}

export default showHelpMessage