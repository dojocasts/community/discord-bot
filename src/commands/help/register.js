export default [
    {
        command: '!help',
        description: 'Toont dit bericht',
    },
    {
        command: '!cat',
        description: 'Verstuurt een foto of gif van een kat.',
    },
    {
        command: '!dog',
        description: 'Verstuurt een foto of gif van een hond.',
    },
    {
        command: '!ipv6excuse',
        description: 'Toont een slecht excuus om geen IPv6 te gebruiken.'
    },
    {
        command: '!doubt @User:OPTIONAL',
        description: 'Stuurt een "ik betwijfel dat"/"doubt" afbeelding, je kan ook iemand taggen.',
    },
    {
        command: '!coinflip',
        description: 'Kop of munt?'
    },
    {
        command: '!eightball <VRAAG>',
        description: 'Antwoord ja of nee?',
    }
]