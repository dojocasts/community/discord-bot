import showHelpMessage from './help/index'
import getCatPictureCommand from './sigfriedseldeslachts/catPictureApi'
import getDogPictureCommand from './sigfriedseldeslachts/dogPictureApi'
import getIPv6Excuse from './sigfriedseldeslachts/ipv6excuse'
import showDoubtImage from './sigfriedseldeslachts/doubtImage'
import wutImage from './sigfriedseldeslachts/wutImage'
import coinFlipCommand from './sigfriedseldeslachts/coinflip'
import eightBallCommand from './sigfriedseldeslachts/eightball'
import eightball from './sigfriedseldeslachts/eightball';

/**
 * @description Place your command functions in here
 * @param {object} message
 * @param {object} client
 * @returns void
 */
const callFunctions = async (message, client) => {
    showHelpMessage(message)

    /**
     * Add your command functions below
     */
    getCatPictureCommand(message)
    getDogPictureCommand(message)
    getIPv6Excuse(message)
    showDoubtImage(message)
    wutImage(message)
    coinFlipCommand(message)
    eightball(message)
}

export default callFunctions