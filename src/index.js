/**
 * DO NOT CHANGE ANYTHING IN THIS FILE
 * 
 * @author Sigfried Seldeslachts
 */
require('dotenv').config()

/**
 * Import files
 */
import client from './init/client'
import registerCommands from './commands/register'

/**
 * Register the commands
 */
client.on('message', (message) => {
    registerCommands(message, client)
})

/**
 * Server greeting
 */
client.on('guildMemberAdd', (member) => {
    const channel = member.guild.channels.find((channel) => {
        channel.name === 'main_chat'
    })

    if (channel) {
        channel.send(`Welkom ${member} op de server!`)
    }
})


/**
 * Login
 */
client.login(process.env.DISCORD_TOKEN)