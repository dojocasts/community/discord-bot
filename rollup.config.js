import babel from 'rollup-plugin-babel';
import babelrc from 'babelrc-rollup';

export default {
    entry: 'dist/index.js',
    dest: 'server_production.js',
    plugins: [
        babel(babelrc())
    ]
};